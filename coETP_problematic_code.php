    <!-- Add the sidebars background. This div must be placed immediately after the control sidebar -->
        <aside class="main-sidebar sidebar-person special-sidebar">
            <!-- sidebar -->
            <section class="sidebar" style="height: auto;">
                <div id="phonecall-box" on-call-modal="false" class="container container-background py-20">
                    <div id="inactive-phone-box" class="row">
                        <h6 id="phone-head" class="col-12 encabezado" style="display:none;"></h6>
                        <div class="col-9 col-sm-9 col-lg-9 col-md-9 pr-0 ml-5">
                        <input id="phone-number" type="text" class="form-control" placeholder="Núm. 10 dígitos" maxlength="10" autocomplete="off">
                        </div>
                        <span class="input-group-btn col-2 col-sm-2 col-md-2 col-lg-2 pl-0">
                        <button id="out-phonecall" class="answer-button btn bg-olive" style="" disabled>
                            <i data-count="0" class="fa fa-phone" style=""></i>
                        </button>
                        </span>

                    </div>
                    <div id="active-phone-box" class="row justify-content-center"  style="display:none">
                        <div class="col-12 col-sm-12 col-lg-12 col-md-12 encabezado ">
                        <div id="call-phone-lbl" class="mb-10" style="font-size: 16px;font-weight: 600;"></div>
                        <span id="call-info-lbl" style="color: #86959d;"></span>
                        </div>
                        <hr class="hr-phonecall-desc">

                        <div id="answer-btn-active-box" class="col-4 col-sm-4 col-md-4 col-lg-4" style="display:none">
                        <button class="answer-button btn bg-olive" style="width: 100%;" disabled>
                            <i data-count="0" class="fa fa-phone" style=""></i>
                        </button>
                        </div>

                        <div id="hangup-btn-active-box" class="col-4 col-sm-4 col-md-4 col-lg-4 ">
                        <button class="hangup-button btn btn-danger" style="width: 100%;" disabled>
                            <i data-count="0" class="mdi mdi-phone-hangup" style=""></i>
                        </button>
                        </div>

                        <div id="mute-btn-active-box" class="col-4 col-sm-4 col-md-4 col-lg-4 ">
                        <button id="mute-button" class="mute-button btn  btn-warning" style="width: 100%;" disabled>
                            <i id="mute-icon" data-count="0" class="fa fa-microphone" style=""></i>
                        </button>
                        </div>
                    </div>
                </div>
                <div class="body-wrapper">
                    <?php $i = 0; ?>
                    @foreach ($dates as $key => $value)
                    <div id="bagde-{{ $i }}" class="container-bagde-index">
                        <div class="fake-badge">
                            <span class="fake-badge-index">{{ $key }}</span>
                        </div>
                    </div>
                    <?php
                      $j = 1;
                      $array_size = count($value);


                    ?>
                        @foreach ($value as $item)
                        <div callsid="call_sid-{{ $item['call_sid']}}" id="accordion acordion-first" X="padre-{{ $item['id'] ?? '' }}">
                            <span>
                                <li class="list-group-item list-group-item-sin-border" id="list-{{ $item['id'] ?? '' }}" data-toggle="collapse" href="#collapseExample" call-direction="{{ $item['direction'] ?? '' }}" aria-expanded="false" aria-controls="#collapse-{{ $item['id'] ?? '' }}" element-id="{{ $item['id'] ?? '' }}" onClick="styles($(this))">
                                    <div class="container-list-calls" id="info-section-{{ $item['id'] ?? '' }}">
                                        <a class="btn-no-padding" data-toggle="collapse" data-target="#collapse-{{ $item['id'] ?? '' }}" aria-expanded="false" aria-controls="collapse-{{ $item['id'] ?? '' }}">
                                            <div class="container-status-icon">
                                                <img class="img-status-call" src="img-oficial/{{ $item['end_status'] ?? '' }}-{{ $item['direction']?? '' }}.svg" />
                                            </div>
                                            <div class="container-num-hr">
                                            <div class="container-num" id="call_sid-{{ $item['call_sid']}}">
                                                @if( $item['direction'] == 'inbound')

                                                <span class="num-phone">{{ $item['customer_name'] ??  $item['from'] }} </span>
                                                @else
                                                <span class="num-phone">{{ $item['customer_name'] ??  $item['to'] }} </span>
                                                @endif
                                            </div>
                                            <div class="container-hr-call">
                                                <span class="hr-call">
                                                @if( isset($item['created_at']) )
                                                    {{ $newDate_lbl = date('G:i a', strtotime($item['created_at'])) }}
                                                @endif
                                                </span>
                                            </div>
                                            </div>
                                            <div class="container-status-description">
                                                <div class=container-status>
                                                    <span class="status-description">
                                                    @if( isset($item['end_status']) )
                                                        {{ $array_lbl[$item['end_status']] }}
                                                    @else
                                                        'N/A'
                                                    @endif
                                                    </span>
                                                </div>
                                                <div class="container-badge">
                                                @if(isset($item['end_status']))
                                                    <span class="badge badge-{{ $array_lbl_status[$item['end_status']] ?? 'dark-m' }} float-right">
                                                    {{  $array_lbl[$item['through']] ?? 'N/A' }}
                                                    {{  $array_selection_lbl[$item['digits']] ?? 'None' }}
                                                    </span>
                                                @endif
                                                </div>
                                                <div class="container-agent">
                                                <span class=agent></span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <!--
                                    @if ($j < $array_size)
                                    <hr class="hr-line">
                                    @endif-->
                                    <?php $j++; ?>
                                </li>
                            </span>
                            <div class="collapse" id="collapse-{{ $item['id'] ?? '' }}" data-parent=".body-wrapper" role="button">
                                @if ( $item['RecordingUrl'] != '' )
                                <div class="holder-parent">
                                    <div class="audio green-audio-player">
                                        <div class="play-pause-btn">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="20" viewBox="0 0 18 24">
                                                <path fill="#1e88e5" fill-rule="evenodd" d="M18 12L0 24V0" class="play-pause-icon" id="playPause"/>
                                            </svg>
                                        </div>
                                        <audio crossorigin>
                                            <source src={{ $item['RecordingUrl'] ?? '' }} type="audio/mpeg">
                                        </audio>
                                    </div>
                                </div>
                                @endif
                                @php
                                //var_dump($item);
                                @endphp
                                <br>
                                <div class="btn-toolbar justify-content-center" role="toolbar" aria-label="Toolbar with button groups">
                                    <div class="btn-group " role="group" aria-label="first group">
                                        <button class="sec-phonecall-btn btn bg-olive mx-5" id="llamar-{{ $item['id'] ?? '' }}" type="button" phone-num="@if( $item['direction'] == 'inbound') {{ $item['from']}}@else{{ $item['to']}}@endif">
                                            <i data-count="0" class="fa fa-phone" style="disable"></i>
                                        </button>
                                    </div>
                                    <div class="btn-group" role="group" aria-label="second group">
                                        <button class="btn btn-success mx-5" type="button">
                                            <i class="bi bi-whatsapp">
                                            </i>
                                        </button>
                                    </div>
                                    <div class="btn-group" role="group" aria-label="Third group">
                                        <button class="btn btn-dark mx-5" type="button">
                                        <i class="bi bi-info-circle-fill"></i>
                                        </i>
                                        </button>
                                    </div>
                                </div>
                                <br>
                            </div>
                        </div>
                        @endforeach
                    @endforeach
                </div>
            </section>
        </aside>

        <!-- cambia el color de background de las llamadas nuevas y seleccionadas -->
        <script>
            function styles(element) {
                console.log('#info-section-' + element.attr('element-id'));
                console.log('#padre-' + element.attr('element-id'));

                $('.container-list-calls.active').removeClass('active');
                $('.card-body.text-center.active').removeClass('active');

                $('#info-section-' + element.attr('element-id')).addClass("active");
                $('#collapse-section-' + element.attr('element-id')).addClass("active");

            };
        </script>
    </div>

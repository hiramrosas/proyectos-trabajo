@extends('layouts.master')
@section('title', 'Contratos')
@section('contracts-active','menu-open')

@section('subtitle', 'Contratos')

@section('head')
<link rel="stylesheet" href="{{asset('css/sidebar_calls.css')}}">
<script src="{{ asset('/assets/vendor_plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('/assets/vendor_plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('/assets/vendor_plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>

@stop
@section('content')

<div class="box box-default">

  <div id="header" class="box-header with-border">
    <div class="row">
      <div class="col-12">
        <h3 class="box-title">Generación de contrato</h3>
        <button id="precioNuevo" type="button" class="btn btn-sm btn-info float-right" data-toggle="modal" data-target="#modal-refrendo">
            <i class="fa fa-plus"></i>
        </button>
      </div>
    </div>
  </div>
  <div class="box-body">

  <form method="POST" action="{{ route('contracts-proccess') }}">
        @csrf
            <div class="modal-body row pt-4">

                <div class="container col-12">
                    <div class="row">
                        <div class="controls col-12 " >

                          <div class="row ">
                            <div class="col-2">
                                <label for="contract_type"> Tipo de contrato: <span class="danger">*</span> </label>
                                <select class="custom-select form-control required" onchange="$('#contract_id').val($(this).val())" id="contract_type" name="contract_type">
                                  <option id="pawn" value="ETPEMP">Empeño</option>
                                  <option id="buys" value="ETPCOM">Compra</option>
                                  <option id="fixed_deadlines" value="ETPEMP_">Plazos Fijos</option>
                                </select>
                            </div>
                            <div class="col-2">
                              <label> Id de contrato: </label>
                              <input id="contract_id" name="contract_id" type="text" class=" form-control " placeholder="ETPEMP000" style="display:block; width:100%" value="ETPEMP">
                              <div id="contract_id_help" class="help-block"></div>
                            </div>
                            <div class="col-2">
                              <label> Fecha contrato: </label>
                              <input id="contract_date" name="contract_date" type="text" class=" form-control " placeholder="dd/mm/aaaa" style="display:block; width:100%" value="{{date('d/m/Y')}}">
                              <div id="contract_date_help" class="help-block"></div>
                            </div>
                            <div class="col-2">
                                <label> Monto del préstamo: </label>
                                <input id="loan_amount" name="loan_amount" type="text" class=" form-control " placeholder="Digita el monto" style="display:block; width:100%">
                                <div id="loan_amount_help" class="help-block"></div>
                              </div>
                              <div class="col-2">
                                <label> Interés neto: </label>
                                <select class="custom-select form-control required" id="net_fee" name="net_fee">
                                    <option value="0.13">13 % - Normal</option>
                                    <option value="0.16">16 % - Especial</option>
                                  </select>
                              </div>
                              <div class="col-2">
                                <label for="fixed_term_months"> No. de meses: </label>
                                <select disabled class="custom-select form-control required" id="fixed_term_months" name="fixed_term_months">
                                  <option selected value="1">1</option>
                                  <option value="2">2</option>
                                  <option value="3">3</option>
                                  <option value="4">4</option>
                                  <option value="5">5</option>
                                  <option value="6">6</option>
                                  <option value="7">7</option>
                                  <option value="8">8</option>
                                  <option value="9">9</option>
                                  <option value="10">10</option>
                                  <option value="11">11</option>
                                  <option value="12">12</option>
                                </select>
                            </div>
                          </div>

                          <hr style="margin: 1.5rem 0rem 1rem 0rem;">
                          <div class="row">
                            <div class="col-12"><h5 class="box-title text-lighter">Consumidor</h5></div>

                            <div class="col-4">
                              <label> Nombre completo: </label>
                              <input id="contract_full_name" name="contract_full_name" type="text" class="form-control " placeholder="Nombre y apellidos" style="display:block; width:100%" value="Federico Martinez Gutierrez Fernandez">
                              <div id="contract_full_name_help" class="help-block"></div>
                            </div>
                            <div class="col-4">
                              <label> Teléfono: </label>
                              <input id="contract_phonenumber" name="contract_phonenumber" type="text" class="form-control " placeholder="Teléfono" style="display:block; width:100%" value="+525620701358">
                              <div id="contract_phonenumber_help" class="help-block"></div>
                            </div>
                            <div class="col-4">
                              <label> Email: </label>
                              <input id="contract_email" name="contract_email" type="text" class="form-control " placeholder="user@email.com" style="display:block; width:100%" value="correo_electronico56@gmail.com">
                              <div id="contract_email_help" class="help-block"></div>
                            </div>

                          </div>
                          <div class="row mt-10">
                            <div class="col-6">
                              <label> Dirección completa: </label>
                              <input id="contract_address" name="contract_address" maxlength="80" type="text" class="form-control " placeholder="Calle, No. Int., No. Ext., Colonia, Delegación, Estado, Pais, CP" style="display:block; width:100%" value="Calle, No. Int., No. Ext., Colonia, Delegación, Estado, Pais, CP">
                              <div id="contract_address_help" class="help-block"></div>
                            </div>

                            <div class="col-3 ">
                              <label for="nat_id_type"> Identificación: <span class="danger">*</span> </label>
                              <select class="custom-select form-control required" onchange="" id="nat_id_type" name="nat_id_type">
                                <option value="INE">INE</option>
                                <option value="Pasaporte">Pasaporte</option>
                                <option value="Ced. migratoria">Ced. migratoria</option>
                                <option value="Licencia">Licencia</option>
                                <option value="Cartilla">Cartilla</option>
                                <option value="Cédula profesional">Cédula profesional</option>
                                <option value="Cred. INAPAM">Cred. INAPAM</option>
                              </select>
                            </div>
                            <div class="col-3">
                              <label> No./Id.:</label>
                              <input id="nat_id_number" name="nat_id_number" type="text" class="form-control " placeholder="#" style="display:block; width:100%" value="1221431324521343">
                              <div id="nat_id_number_help" class="help-block"></div>
                            </div>

                          </div>
                          <hr style="margin: 1.5rem 0rem 1rem 0rem;">

                          <div class="row mt-10">
                            <div class="col-12"><h5 class="box-title">Beneficiario</h5></div>

                            <div class="col-4">
                              <label> Nombre completo:</label>
                              <input id="beneficiary_full_name" name="beneficiary_full_name" type="text" class="form-control " placeholder="Nombre y apellidos" style="display:block; width:100%" value="Federica Martinez Gutierrez Fernandez">
                              <div id="beneficiary_full_name_help" class="help-block"></div>
                            </div>
                            <div class="col-8">
                              <label> Dirección completa:</label>
                              <input id="beneficiary_address" name="beneficiary_address" type="text" class="form-control " placeholder="Calle, No. Int., No. Ext., Colonia, Delegación, Estado, Pais, CP" style="display:block; width:100%" value="Calle, No. Int., No. Ext., Colonia, Delegación, Estado, Pais, CP">
                              <div id="beneficiary_address_help" class="help-block"></div>
                            </div>
                          </div>

                        </div>


                    </div>
                </div>

            </div>
            <div class="text-center col-12">
              <button id="modalForm" type="submit" class=" btn btn-bold btn-pure btn-info col-10">Generar contrato</button>
            </div>
          </form>
          <hr>
          @isset ($url_pdf)
          <iframe src="{{$url_pdf}}" width="100%" height="1000px"></iframe>
          @endisset

  </div>
</div>

<script type="text/javascript">

  $(document).ready(function(){
    $('#contract_date').datepicker({
      autoclose: true,
      orientation: "bottom",
      format: 'dd/mm/yyyy',
    });

  });

</script>

<script>
    $('#contract_type').change(function(){
        if((this).value != 'ETPEMP_')
          $('#fixed_term_months').attr('disabled','disabled')
        else
          $('#fixed_term_months').removeAttr('disabled')
      });
</script>
@stop

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="session-email" content="{{ Auth::user()->email }}">

    <title>Control de Operaciones</title>

    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="{{ asset('/assets/vendor_components/bootstrap/dist/css/bootstrap.css') }}">

    <!-- Bootstrap 4.0 icon-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">

    <!-- Bootstrap Extended 4.0-->
    <link rel="stylesheet" href="{{ asset('/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css') }}">

    <!-- theme style -->
    <link rel="stylesheet" href="{{ asset('css/master_style.css') }}">

    <!-- MinimalLite Admin skins. choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
    <!-- <link rel="stylesheet" href="{{ asset('css/skins/_all-skins.css') }}"> -->
    <link rel="stylesheet" href="{{ asset('css/skin.css') }}">

    <!-- Morris.js charts CSS -->
    <link href="{{ asset('/assets/vendor_components/morris.js/morris.css') }}" rel="stylesheet" />

    <!-- Vector CSS -->
    <link href="{{ asset('/assets/vendor_components/jvectormap/lib2/jquery-jvectormap-2.0.2.css') }}"
        rel="stylesheet" />

    <!-- date picker -->
    <link rel="stylesheet"
        href="{{ asset('/assets/vendor_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css') }}">

    <!-- daterange picker
    <link rel="stylesheet" href="{{ asset('/assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css') }}">-->

    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{ asset('/assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.css') }}">

    <!-- jQuery 3 -->
    <script src="{{ asset('/assets/vendor_components/jquery/dist/jquery.js') }}"></script>

    <!-- jQuery UI 1.11.4 -->
    <script src="{{ asset('/assets/vendor_components/jquery-ui/jquery-ui.js') }}"></script>


    <script src="{{ asset('/js/jquery.iframe-transport.js') }}"></script>


    <script src="{{ asset('/js/jquery.fileupload.js') }}"></script>

    <!-- toast -->
    <link href="{{ asset('/assets/vendor_components/jquery-toast-plugin-master/src/jquery.toast.css') }}"
        rel="stylesheet">
    <!-- toast plug in -->
    <script src="{{ asset('/assets/vendor_components/jquery-toast-plugin-master/src/jquery.toast.js') }}"></script>
    <script src="{{ asset('/js/pages/toastr.js') }}"></script>

    <script src="{{ asset('/assets/vendor_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <!-- start - This is for export functionality only -->
    <script
        src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}">
    </script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}">
    </script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}">
    </script>
    <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}">
    </script>
    <script
        src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Responsive/js/dataTables.responsive.min.js') }}">
    </script>
    <script
        src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/FixedHeader/js/dataTables.fixedHeader.min.js') }}">
    </script>

    <script src="{{ asset('/js/pages/data-table.js') }}"></script>
    <script src="{{ asset('/js/calculator.js') }}"></script>
    <script src="{{ asset('/js/ui-avatar-svg.umd.js') }}"></script>

    <script src="{{ asset('/js/green-audio-player.min.js') }}"></script>


    <!-- date switch  -->
    <link rel="stylesheet" href="{{ asset('/assets/vendor_components/bootstrap-switch/switch.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
 <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js')}}"></script>
 <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js')}}"></script>
  <![endif]-->
    @yield('head')
    <script src="https://js.pusher.com/3.1/pusher.min.js"></script>

</head>

<body class="hold-transition skin-green-light sidebar-mini sidebar-collapse">

    <div class="wrapper">
        <header class="main-header">
            <!-- Logo -->
            <a href="{{ route('home') }}" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <b class="logo-mini">
                    <span class="light-logo"><img src="{{ asset('images/logo-sm.png') }}"></span>
                </b>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg">
                    <img src="{{ asset('images/logo-lg.png') }}" alt="logo" class="light-logo">
                </span>
            </a>



            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li>
                            <span id="call-status-lbl"
                                style="height: 100%; color: #FFF; float: left; padding: 20px ;"></span>
                        </li>

                        <li>




                            <!-- end -->

                        </li>
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-cog "></i>
                            </a>
                            <ul class="dropdown-menu scale-up">
                                <!-- User image -->
                                <li class="user-header">
                                    {{ Auth::user()->name }}
                                    <small class="mb-3"
                                        style="overflow:hidden">{{ Auth::user()->email }}</small>
                                </li>
                                <!-- Menu Body -->
                                <li class="user-body">
                                    <div class="row no-gutters">
                                        <div role="separator" class="divider col-12"></div>
                                        <div role="separator" class="divider col-12"></div>
                                        <div class="col-12 text-left">
                                            <a href="{{ route('users.edit', Auth::user()) }}"><i
                                                    class="fa fa-user-circle-o"></i>Editar perfil</a>
                                        </div>
                                        <div class="col-12 text-left">
                                            <a href="{{ route('logout') }}"
                                                onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i
                                                    class="fa fa-power-off"></i>Cerrar sesion</a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                style="display: none;">
                                                @csrf
                                            </form>
                                        </div>
                                        <!-- /.row -->
                                </li>
                            </ul>
                        </li>
                        <!-- Cambiar color de tema  -->
                        <!-- <li>
              <a href="#" data-toggle="control-sidebar"><i class="fa fa-cog fa-spin"></i></a>
            </li> -->
                    </ul>
                </div>
            </nav>


        </header>

        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">

                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="@yield('home-active')">
                        <a href="{{ route('home') }}">
                            <i class="fa fa-calculator"></i>
                            <span>Avalúos</span>
                        </a>
                    </li>
                    <li class="@yield('cat-active')">
                        <a href="{{ route('Admin') }}">
                            <i class="fa fa-navicon"></i>
                            <span>Categorías</span>
                        </a>
                    </li>
                    <li class="@yield('chat-active')">
                        <a href="{{ route('chat') }}">
                            <i id="notification-gral-icon" data-count="0" class="fa fa-whatsapp"></i>
                            <span>Chat</span>
                        </a>
                    </li>
                    <li class="@yield('precios-active')">
                        <a href="{{ route('precios') }}">
                            <i class="fa fa-list-alt"></i>
                            <span>Precios</span>
                        </a>
                    </li>
                    <li class="@yield('buscador-active')">
                        <a href="{{ route('buscador') }}">
                            <i class="fa fa-search"></i>
                            <span>Buscador</span>
                        </a>
                    </li>
                    <li class="@yield('refrendos-active')">
                        <a href="{{ route('refrendos') }}">
                            <i class="fa fa-money"></i>
                            <span>Refrendos</span>
                        </a>
                    </li>
                    <li class="@yield('contracts-active')">
                        <a href="{{ route('contracts') }}">
                            <i class="fa fa-file-text"></i>
                            <span>Contratos</span>
                        </a>
                    </li>
                </ul>
            </section>
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header)
            <section class="content-header">
                <h1>
                Operaciones
                <small>@yield('title')</small>
                </h1>
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="breadcrumb-item active">@yield('subtitle')</li>
                </ol>
            </section>
            -->

            <!-- Contenido para extender -->
            <section class="content">
                @yield('content')
            </section>
            <!-- /.content -->

        </div>

        <!-- /.control-sidebar -->
        <!-- Add the sidebars background. This div must be placed immediately after the control sidebar -->
        <aside class="main-sidebar sidebar-person special-sidebar">
            <!-- sidebar -->
            <section class="sidebar" style="height: auto;">
                <div id="phonecall-box" on-call-modal="false" class="container container-background py-20">
                    <div id="inactive-phone-box" class="row">
                        <h6 id="phone-head" class="col-12 encabezado" style="display:none;"></h6>
                        <div class="col-9 col-sm-9 col-lg-9 col-md-9 pr-0 ml-5">
                            <input id="phone-number" type="text" class="form-control" placeholder="Núm. 10 dígitos"
                                maxlength="10" autocomplete="off">
                        </div>
                        <span class="input-group-btn col-2 col-sm-2 col-md-2 col-lg-2 pl-0">
                            <button id="out-phonecall" class="answer-button btn bg-olive" style="" disabled>
                                <i data-count="0" class="fa fa-phone" style=""></i>
                            </button>
                        </span>

                    </div>
                    <div id="active-phone-box" class="row justify-content-center" style="display:none">
                        <div class="col-12 col-sm-12 col-lg-12 col-md-12 encabezado ">
                            <div id="call-phone-lbl" class="mb-10" style="font-size: 16px;font-weight: 600;">
                            </div>
                            <span id="call-info-lbl" style="color: #86959d;"></span>
                        </div>
                        <hr class="hr-phonecall-desc">

                        <div id="answer-btn-active-box" class="col-4 col-sm-4 col-md-4 col-lg-4" style="display:none">
                            <button class="answer-button btn bg-olive" style="width: 100%;" disabled>
                                <i data-count="0" class="fa fa-phone" style=""></i>
                            </button>
                        </div>

                        <div id="hangup-btn-active-box" class="col-4 col-sm-4 col-md-4 col-lg-4 ">
                            <button class="hangup-button btn btn-danger" style="width: 100%;" disabled>
                                <i data-count="0" class="mdi mdi-phone-hangup" style=""></i>
                            </button>
                        </div>

                        <div id="mute-btn-active-box" class="col-4 col-sm-4 col-md-4 col-lg-4 ">
                            <button id="mute-button" class="mute-button btn  btn-warning" style="width: 100%;" disabled>
                                <i id="mute-icon" data-count="0" class="fa fa-microphone" style=""></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="body-wrapper">
                    <?php $i = 0; ?>
                    @foreach ($dates as $key => $value)
                        <div id="bagde-{{ $i }}" class="container-bagde-index">
                            <div class="fake-badge">
                                <span class="fake-badge-index">{{ $key }}</span>
                            </div>
                        </div>
                        <?php
                        $j = 1;
                        $array_size = count($value);

                        ?>
                        @foreach ($value as $item)
                            <div callsid="call_sid-{{ $item['call_sid'] }}" id="accordion acordion-first"
                                X="padre-{{ $item['id'] ?? '' }}">
                                <span>
                                    <li class="list-group-item list-group-item-sin-border"
                                        id="list-{{ $item['id'] ?? '' }}" data-toggle="collapse"
                                        href="#collapseExample" call-direction="{{ $item['direction'] ?? '' }}"
                                        aria-expanded="false" aria-controls="#collapse-{{ $item['id'] ?? '' }}"
                                        element-id="{{ $item['id'] ?? '' }}" onClick="styles($(this))">
                                        <div class="container-list-calls" id="info-section-{{ $item['id'] ?? '' }}">
                                            <a class="btn-no-padding" data-toggle="collapse"
                                                data-target="#collapse-{{ $item['id'] ?? '' }}" aria-expanded="false"
                                                aria-controls="collapse-{{ $item['id'] ?? '' }}">
                                                <div class="container-status-icon">
                                                    <img class="img-status-call"
                                                        src="img-oficial/{{ $item['end_status'] ?? '' }}-{{ $item['direction'] ?? '' }}.svg" />
                                                </div>
                                                <div class="container-num-hr">
                                                    <div class="container-num" id="call_sid-{{ $item['call_sid'] }}">
                                                        @if ($item['direction'] == 'inbound')
                                                            <span
                                                                class="num-phone">{{ $item['customer_name'] ?? $item['from'] }}
                                                            </span>
                                                        @else
                                                            <span
                                                                class="num-phone">{{ $item['customer_name'] ?? $item['to'] }}
                                                            </span>
                                                        @endif
                                                    </div>
                                                    <div class="container-hr-call">
                                                        <span class="hr-call">
                                                            @if (isset($item['created_at']))
                                                                {{ $newDate_lbl = date('G:i a', strtotime($item['created_at'])) }}
                                                            @endif
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="container-status-description">
                                                    <div class=container-status>
                                                        <span class="status-description">
                                                            @if (isset($item['end_status']))
                                                                {{ $array_lbl[$item['end_status']] }}
                                                            @else
                                                                'N/A'
                                                            @endif
                                                        </span>
                                                    </div>
                                                    <div class="container-badge">
                                                        @if (isset($item['end_status']))
                                                            <span
                                                                class="badge badge-{{ $array_lbl_status[$item['end_status']] ?? 'dark-m' }} float-right">
                                                                {{ $array_lbl[$item['through']] ?? 'N/A' }}
                                                                {{ $array_selection_lbl[$item['digits']] ?? 'None' }}
                                                            </span>
                                                        @endif
                                                    </div>
                                                    <div class="container-agent">
                                                        <span class=agent></span>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <!--
                                    @if ($j < $array_size)
<hr class="hr-line">
@endif-->
                                        <?php $j++; ?>
                                    </li>
                                </span>
                                <div class="collapse" id="collapse-{{ $item['id'] ?? '' }}"
                                    data-parent=".body-wrapper" role="button">
                                    <div class="alert sidebar-info-bg alert-dismissible">
                                        <h5><i class="icon fa fa-user-circle"></i> Info</h5>
                                        <span class="capitalize"> Nombre: {{ $item['customer_name'] }}
                                        </span><br>
                                        @php
                                            if ($item['called'] == '+525568269407') {
                                                $phone_number = substr($item['from'], 3);
                                            } else {
                                                $phone_number = substr($item['to'], 3);
                                            }

                                            $curl = curl_init();
                                            curl_setopt_array($curl, array(
                                            CURLOPT_URL => env('API_ROUTE').'request/get-all/'.$phone_number,
                                            CURLOPT_RETURNTRANSFER => true,
                                            CURLOPT_ENCODING => '',
                                            CURLOPT_MAXREDIRS => 10,
                                            CURLOPT_TIMEOUT => 0,
                                            CURLOPT_FOLLOWLOCATION => true,
                                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                            CURLOPT_CUSTOMREQUEST => 'GET',
                                            ));

                                            $response = curl_exec($curl);
                                            curl_close($curl);
                                            $data = json_decode($response);
                                            $email = $data[0]->client->email;

                                        @endphp
                                        <span>Teléfono: {{ $phone_number }} </span><br>
                                        <span > {{ $email }} </span><br>
                                    </div>
                                    @if ($item['RecordingUrl'] != '')
                                        <div class="holder-parent">
                                            <div class="audio green-audio-player">
                                                <div class="play-pause-btn">
                                                    <svg xmlns="https://www.w3.org/2000/svg" width="16" height="20"
                                                        viewBox="0 0 18 24">
                                                        <path fill="#1e88e5" fill-rule="evenodd" d="M18 12L0 24V0"
                                                            class="play-pause-icon" id="playPause" />
                                                    </svg>
                                                </div>
                                                <audio crossorigin>
                                                    <source src={{ $item['RecordingUrl'] ?? '' }} type="audio/mpeg">
                                                </audio>
                                            </div>
                                        </div>
                                    @endif
                                    @php
                                        //var_dump($item);
                                    @endphp
                                    <br>
                                    <div class="btn-toolbar justify-content-center" role="toolbar"
                                        aria-label="Toolbar with button groups">
                                        <div class="btn-group " role="group" aria-label="first group">
                                            <button class="sec-phonecall-btn btn bg-olive mx-5"
                                                id="llamar-{{ $item['id'] ?? '' }}" type="button"
                                                phone-num="@if ($item['direction'] == 'inbound') {{ $item['from'] }}@else{{ $item['to'] }} @endif">
                                                <i data-count="0" class="fa fa-phone" style="disable"></i>
                                            </button>
                                        </div>
                                        <div class="btn-group" role="group" aria-label="second group">
                                            <button disabled class="btn btn-success mx-5" type="button">
                                                <i class="bi bi-whatsapp">
                                                </i>
                                            </button>
                                        </div>
                                        <div class="btn-group" role="group" aria-label="Third group">
                                            <button disabled class="btn btn-dark mx-5" type="button">
                                                <i class="bi bi-info-circle-fill"></i>
                                                </i>
                                            </button>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            </div>
                        @endforeach
                    @endforeach
                </div>
            </section>
        </aside>

        <!-- cambia el color de background de las llamadas nuevas y seleccionadas -->
        <script>
            function styles(element) {
                console.log('#info-section-' + element.attr('element-id'));
                console.log('#padre-' + element.attr('element-id'));

                $('.container-list-calls.active').removeClass('active');
                $('.card-body.text-center.active').removeClass('active');

                $('#info-section-' + element.attr('element-id')).addClass("active");
                $('#collapse-section-' + element.attr('element-id')).addClass("active");

            };
        </script>
    </div>


    <!-- ./wrapper -->

    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>

    <!-- popper -->
    <script src="{{ asset('/assets/vendor_components/popper/dist/popper.min.js') }}"></script>

    <!-- Bootstrap 4.0-->
    <script src="{{ asset('/assets/vendor_components/bootstrap/dist/js/bootstrap.js') }}"></script>

    <!-- Morris.js charts -->
    <script src="{{ asset('/assets/vendor_components/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor_components/morris.js/morris.min.js') }}"></script>

    <!-- ChartJS -->
    <script src="{{ asset('/assets/vendor_components/chart-js/chart.js') }}"></script>

    <!-- Sparkline -->
    <script src="{{ asset('/assets/vendor_components/jquery-sparkline/dist/jquery.sparkline.js') }}"></script>

    <!-- Slimscroll -->
    <script src="{{ asset('/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

    <!-- FastClick -->
    <script src="{{ asset('/assets/vendor_components/fastclick/lib/fastclick.js') }}"></script>

    <!-- peity -->
    <script src="{{ asset('/assets/vendor_components/jquery.peity/jquery.peity.js') }}"></script>

    <!-- Vector map JavaScript -->
    <script src="{{ asset('/assets/vendor_components/jvectormap/lib2/jquery-jvectormap-2.0.2.min.js') }}"></script>
    <script src="{{ asset('/assets/vendor_components/jvectormap/lib2/jquery-jvectormap-us-aea-en.js') }}"></script>

    <!-- Datepicker -->
    <script src="{{ asset('/assets/vendor_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}">
    </script>

    <!-- MinimalLite Admin App -->
    <script src="{{ asset('/js/template.js') }}"></script>

    <!-- MinimalLite Admin dashboard demo (This is only for demo purposes) -->
    <script src="{{ asset('/js/pages/dashboard.js') }}"></script>

    <!-- MinimalLite Admin for demo purposes -->
    <script src="{{ asset('/js/demo.js') }}"></script>

    <script src="/js/vendor.js"></script>

    @yield('javascript')

    <script src="/js/manifest.js"></script>

    <script src="/js/twilio.js"></script>

    <!-- moment js -->

    <script type="text/javascript">
        var lbl_status = {};
        @foreach ($array_lbl as $key => $value)
            {!! 'lbl_status["' . $key . '"]= "' . $value . '";' !!}
        @endforeach

        var lbl_status_color = {};

        @foreach ($array_lbl_status as $key => $value)
            {!! 'lbl_status_color["' . $key . '"]= "' . $value . '";' !!}
        @endforeach
        const Device = Twilio.Device;
        var device = null;
        var callStatus = $("#call-status");
        var callStatusLabel = $("#call-status-lbl");
        var callPhoneLabel = $("#call-phone-lbl");
        var callInfoLabel = $("#call-info-lbl");
        var answerButton = $(".answer-button");
        var callSupportButton = $(".call-support-button");
        var hangUpButton = $(".hangup-button");
        var callCustomerButtons = $(".call-customer-button");
        var refrechTokenButton = $("#refresh-btn");
        var customerDetailButton = $("#customer-detail");
        var muteButton = $(".mute-button");
        var muteIcon = $('#mute-icon');
        var phoneNumberInput = $('#phone-number');
        var refreshButton = $('#refresh-button');
        var inactiveBoxCall = $('#inactive-phone-box');
        var activeBoxCall = $('#active-phone-box');
        var phonecallBox = $('#phonecall-box');
        var answerBtnActiveBox = $('#answer-btn-active-box');
        var hangupBtnActiveBox = $('#hangup-btn-active-box');
        var muteBtnActiveBox = $('#mute-btn-active-box');


        var pusher = null;
        var channel_wa = null;
        var channel_request = null;



        async function getTokenViaAjax() {
            const result = await $.post("/phonecall/token", {
                forPage: window.location.pathname
            }).done(function(data) {}).fail(function() {
                return "error";
            });

            return result.token;
        }

        const ttl = 600000; // 10 minutes
        const refreshBuffer = 30000; // 30 seconds



        $(document).ready(function() {


            // Jquery BOTON LLAMAR en sidebar
            $(".sec-phonecall-btn").click(function() {
                console.log($(this).attr('phone-num'));
                let phoneNumber = $(this).attr('phone-num');
                let phoneModify = phoneNumber.substr(3);
                phoneNumberInput.val(phoneModify);
                phoneNumberInput.click();
                $('#out-phonecall').click();


            });
            // end Jquery BOTON LLAMAR en sidebar
            //  Javascript del reproductor

            GreenAudioPlayer.init({
                selector: '.green-audio-player', // inits Green Audio Player on each audio container that has class "player"
                stopOthersOnPlay: true
            });

            var audioPlayer = document.querySelector('.green-audio-player');
            var progress = audioPlayer.querySelector('.progress');
            var currentTime = audioPlayer.querySelector('.controls__current-time');

            var player = audioPlayer.querySelector('audio');

            function updateProgress() {
                var current = player.currentTime;
                var percent = (current / player.duration) * 100;
                progress.style.width = percent + '%';

                currentTime.textContent = formatTime(current);
            }
            // End Javascript del reproductor


            //
            updateCallStatus("Servicio activo");

            setupClient();
            setupNotification();

            phoneNumberInput.on("change paste keyup click", function(e) {
                //console.log($(this).val().length);
                phonecallBox.attr('on-call-modal', 'true');
                refreshButton.prop("disabled", false);

                if ($(this).val().length == 10) {

                    addAnswerClick();
                    answerButton.prop("disabled", false);

                    //console.log("trying change input");
                    //Evento para notificar que son 10 digitos
                    var settingsEvents = {
                        "async": true,
                        "crossDomain": true,
                        "url": "/event/phonecall/" + phoneNumberInput.val(),
                        "method": "GET",
                        "dataType": "JSON"
                    }

                    $.ajax(settingsEvents).done(function(response) {
                        console.log('event triggered');
                    });


                    e = window.event;
                    //console.log(e.keyCode);
                    var keycode = (e.keyCode ? e.keyCode : e.which);
                    if (keycode == '13' && !e.shiftKey) {
                        //console.log('da enter');
                        $('#out-phonecall').click();

                    }
                    //refreshButton.prop("disabled", true);

                } else {
                    answerButton.prop("disabled", true);
                }
            });

            //addAnswerClick();

        });

        function setupNotification() {
            console.log(Notification.permission);
            if (Notification.permission === "granted") {
                console.log('granted');
                //showNotification();
            } else if (Notification.permission !== "denied") {
                console.log('default');
                Notification.requestPermission().then(permission => {
                    if (permission === "granted") {
                        // showNotification();
                    }
                });
            }
        }

        function showNotification(username, phone_number) {
            const notification = new Notification("Llamada: " + username, {
                body: "Número: " + phone_number,
                icon: "https://admin.empenatuprenda.com/images/chat/square.png"
            });

            notification.onclick = (e) => {
                window.focus();
            };
            /*
            notification.onclick = function () {
                window.focus();
            }*/
        }

        function updateCallStatus(status) {
            //console.log(status);
            callStatusLabel.html('');
            callStatus.attr('value', status);
            callStatusLabel.html(status);
        }

        function setupClient() {
            getTokenViaAjax().then(function(result) {
                // Set up the Twilio Client device with the token
                //console.log(data.toke)

                device = new Device(result);
                device.register();
                setupHandlers(device);


                answerButton.click(function() {
                    console.log('ingresa a answerButton.click');

                });


            });

            setInterval(async () => {
                const newToken = await getTokenViaAjax();
                device.updateToken(newToken);
                //console.log('Actualiza token'+newToken);
            }, ttl - refreshBuffer);
        }
        //INCOME PHONE CALL
        var _device_var = null;

        function setupHandlers(device) {
            console.log("setupHandlers()");

            //INCOME PHONE CALL
            device.on('incoming', function(_device) {
                console.log("Device.incoming");
                console.log(_device);
                if ((_device_outcome == null ? true : _device_outcome.status() != "open")) {

                    console.log("Número: " + phoneNumberInput.val().length);

                    if (phoneNumberInput.val().length == 0) {
                        _device_var = _device;
                        //refreshButton.prop("disabled", false);
                        phoneNumberInput.hide();
                        inactiveBoxCall.hide();
                        activeBoxCall.show();
                        customerDetailButton.prop("disabled", false);
                        answerButton.prop("disabled", false);
                        answerBtnActiveBox.show();
                        hangupBtnActiveBox.hide();
                        muteBtnActiveBox.hide();
                        updateCallStatus("Llamando... " + _device.parameters
                            .From); // Set a callback to be executed when the _device is accepted
                        addCustomerInfo((_device.parameters.From).substring(3), _device.customParameters.get(
                            "origin"));

                        _device.on('accept', function(_device_event) {
                            hangUpButton.prop("disabled", false);
                            updateCallStatus("En llamada: " + _device_event.parameters.From);
                            muteButton.prop("disabled", false);
                            answerButton.prop("disabled", false);
                            answerBtnActiveBox.hide();
                            hangupBtnActiveBox.show();
                            muteBtnActiveBox.show();
                            var long_number = "";
                            long_number = (String(_device_event.parameters.From)).substring(3);
                            $('#phone-icon').addClass("text-yellow").addClass("fa-spin");
                            phonecallBox.attr('on-call-modal', 'true');

                            customerDetailButton.click(function() {
                                //console.log('#list-element-'+long_number);
                                //phonecallBox.modal('hide');
                                $('#list-element-521' + long_number).click();
                            });
                            muteButton.click(function() {
                                console.log('click muteButton');
                                if (!_device_event.isMuted()) {
                                    console.log('isMuted');
                                    _device_event.mute(true);
                                    muteIcon.addClass("fa-microphone-slash").removeClass(
                                        "fa-microphone");
                                } else {
                                    console.log('noMuted');
                                    _device_event.mute(false);
                                    muteIcon.addClass("fa-microphone").removeClass(
                                        "fa-microphone-slash");
                                }
                            });

                        });


                        _device.on('cancel', function(_device_event) {
                            console.log('try _device_event.cancel-' + _device_var.status());

                            console.log(_device_event);
                            if (_device_var.status() == "closed") {
                                resetUI();
                            }
                            /*
                            _device_var.disconnect();
                            _device.disconnect();
                            device.disconnectAll();
                            //device.destroy();
                            resetUI();
                            setupClient();
                            */

                        });

                        _device.on('disconnect', function(_device) {
                            console.log('_device.disconnect');
                            resetUI();
                        });
                        answerButton.click(function() {
                            _device.accept();
                            console.log("llamada aceptada");
                        });

                        hangUpButton.click(function() {
                            //_device_event.disconnect();
                            _device_var.disconnect();
                            _device.disconnect();
                            device.disconnectAll();
                            //device.destroy();
                            resetUI();
                            //setupClient();
                        });


                    }
                }
            });
            device.on('register', function(_device) {
                console.log("Device.register");
            });
            device.on('registered', function(_device) {
                console.log("Registered");
            });
            device.on('unregistered', function(_device) {
                console.log("Unregistered");
                //setupClient();
            });
            device.on('error', (twilioError, call) => {
                console.log('An error has occurred: ', twilioError);
            });
            device.on('cancel', function(device) {
                console.log('Device.cancel');
                resetUI();
            });
        }

        function addAnswerClick() {
            removeAnswerClick();
            answerButton.click(function() {
                var settingsEvents = {
                    "async": true,
                    "crossDomain": true,
                    "url": "/event/calling/" + phoneNumberInput.val(),
                    "method": "GET",
                    "dataType": "JSON"
                }
                $.ajax(settingsEvents).done(function(response) {
                    console.log('event calling triggered');
                });
                callCustomer('+52' + phoneNumberInput.val());
            });
        }

        //OUTCOME PHONE CALL
        var connection_var = null;
        var _device_outcome = null;

        function callCustomer(phoneNumber) {
            updateCallStatus(phoneNumber);

            callCustomerButtons.prop("disabled", true);
            callSupportButton.prop("disabled", true);
            hangUpButton.prop("disabled", false);
            muteButton.prop("disabled", false);
            //refreshButton.prop("disabled", true);
            inactiveBoxCall.hide();
            activeBoxCall.show();

            callPhoneLabel.append('<b>' + phoneNumberInput.val() + '</b><br>');
            addCustomerInfo(phoneNumberInput.val(), 'etp');

            connection_var = device.connect({
                params: {
                    phoneNumber: phoneNumber
                }
            });

            console.log(connection_var);

            connection_var.then(function(result) {
                _device_outcome = result;
                answerButton.prop("disabled", true);

                if (result.customParameters.get("phoneNumber")) {
                    updateCallStatus("En llamada: " + result.customParameters.get("phoneNumber"));
                } else {
                    updateCallStatus("In call support");
                }

                muteButton.click(function() {
                    if (!result.isMuted()) {
                        result.mute(true);
                        muteIcon.addClass("fa-microphone-slash").removeClass("fa-microphone");
                    } else {
                        console.log('noMuted');
                        result.mute(false);
                        muteIcon.addClass("fa-microphone").removeClass("fa-microphone-slash");
                    }
                });
                hangUpButton.click(function() {
                    _device_outcome.disconnect();
                    result.disconnect();
                    resetUI();
                });

                result.on('cancel', function(_device) {
                    console.log("Device.cancel");
                });
                result.on('disconnect', function(_device) {
                    console.log("Device.disconnect");
                    resetUI();
                });
                result.on('error', function(_device) {
                    console.log("Device.error");
                });
                result.on('reject', function(_device) {
                    console.log("Device.reject");
                });
                //Mute Properties


            });

        }

        function removeAnswerClick() {
            answerButton.prop("onclick", null).off("click");
        }

        function addCustomerInfo(short_phone, origin_lbl) {
            callInfoLabel.html("");

            var objRequestChatter = {};
            console.log('whatsapp:+521' + short_phone);
            objRequestChatter['owner'] = 'whatsapp:+521' + short_phone;
            var settingsChatter = {
                "async": true,
                "crossDomain": true,
                "url": "https://admin.empenatuprenda.com/api/messages/whatsapp/get-chatter",
                "method": "POST",
                "data": objRequestChatter
            }
            var str_injection = "";
            $.ajax(settingsChatter).done(function(response) {
                str_injection_name = '' + response.name;
                str_injection = '+52' + short_phone + '<br>' + response.email;
                console.log(str_injection);

                if (origin_lbl == "etp" || origin_lbl == "Empeña tu Prenda") {
                    callInfoLabel.append(
                        '<b><img src="https://empenatuprenda.com/favico.ico?v=2" alt="Empeña tu prenda" /> ' +
                        str_injection
                    );
                } else if (origin_lbl == "tsc") {
                    callInfoLabel.append(
                        '<b><img src=".png" alt="TSC" width="21"></img> Tu Siguiente Compra</b><br>' +
                        str_injection
                    );
                } else {
                    callInfoLabel.append(
                        '<b><img src="https://empenatuprenda.com/favico.ico?v=2" alt="Empeña tu prenda" /> ' +
                        str_injection
                    );
                }
                callPhoneLabel.html(str_injection_name);
                showNotification(response.name, '+52' + short_phone);
            });
        }

        // A click on a 'Hang Up' button calls this method
        const hangUp = () => {
            device.disconnectAll();
            //device.destroy();
            //setupClient();
        }

        function resetUI() {
            //hangUp();
            console.log('resetUI()');
            callCustomerButtons.prop("disabled", false);
            callSupportButton.prop("disabled", false);
            answerButton.prop("disabled", true);
            muteButton.prop("disabled", true);
            inactiveBoxCall.show();
            activeBoxCall.hide();
            answerBtnActiveBox.hide();
            hangupBtnActiveBox.show();
            muteBtnActiveBox.show();
            phoneNumberInput.show();
            callInfoLabel.html("");
            callPhoneLabel.html("");
            hangUpButton.prop("disabled", true);
            customerDetailButton.prop("disabled", true);
            $('#phone-icon').removeClass("text-yellow").removeClass("fa-spin");

            muteIcon.addClass("fa-microphone").removeClass("fa-microphone-slash");
            updateCallStatus("Servicio activo");
            //reset click
            removeAnswerClick();
            phoneNumberInput.val("");
            phonecallBox.attr('on-call-modal', 'false');
        }


        var message_element_lbl = "";



        var pusher = new Pusher('80394f1c3e1c8a921b02', {
            cluster: 'us2',
            encrypted: true
        });


        // Subscribe to the channel we specified in our Laravel Event
        var channel_wa = pusher.subscribe('wa-message');
        var channel_request = pusher.subscribe('new-request');
        var channel_phonecalls = pusher.subscribe('phone-calls');


        // Bind a function to a Event (the full Laravel class)
        channel_wa.bind('App\\Events\\PanelNotification', function(element) {

            if (window.location.pathname == "/chat") {
                pusherChat(element);
            }
            //Si el numero/chatter no existe
            if (element.message != null) {

                if ($('#chat-app-' + (element.message.owner).substring(10)).length == 0) {
                    var is_template_message = true;
                }

                //Change number and play notify sound
                if (element.message.owner == element.message.from || is_template_message) {
                    is_template_message = false;
                    $('#notification-gral-icon').addClass('notification');
                    $('#notification-gral-icon').attr('data-count', parseInt($('#notification-gral-icon').attr(
                        'data-count')) + 1);
                }
            }

            if (element.event.type == 'phonecall' && element.event.email != $('meta[name="session-email"]').attr(
                    'content') && element.event.id == phoneNumberInput.val() && phonecallBox.attr(
                    'on-call-modal') == 'false') {
                console.log(element);
                console.log('phonecall');
                resetUI();
            } else if (element.event.type == 'calling' && element.event.email != $('meta[name="session-email"]')
                .attr('content') && element.event.id == phoneNumberInput.val()) {
                console.log(element);
                console.log('calling');
                resetUI();
            }

        });


        channel_request.bind('new-request-notification', function(data) {
            if (window.location.pathname == "/avaluos") {
                pusherValuation(data);
            }


            request = data.request;

            //console.log(request);
            if (request.type == "phonecall-order" && phonecallBox.attr('on-call-modal') == 'false') {
                console.log('from-top');
                //console.log(request);
                $("#call-status-lbl").html();
                inactiveBoxCall.show();
                activeBoxCall.hide();
                callInfoLabel.html("");
                callInfoLabel.append(
                    '<img src="https://empenatuprenda.com/favico.ico?v=2" alt="Empeña tu prenda" /> <br>'
                );
                var str_injection =
                    '- - - - - - - - - - - - - - - ' +
                    '<br>Nombre: ' + request.customer_name +
                    '<br>Telefono: ' + request.customer_phone +
                    '<br>Email: ' + request.customer_email +
                    '<br>- - - - - - - - - - - - - - - <br>' +
                    request.category_lbl + '>' + request.brand_lbl + '>' + request.product_lbl + '>' + request
                    .detail_lbl +
                    '<br>- - - - - - - - - - - - - - - ' +
                    ((request.pawn_offer != null) ? '<br>Prestamo: $' + request.pawn_offer.monthly.loan : '') +
                    ((request.buy_offer != null) ? '<br>Compra: $' + request.buy_offer.amount : '');

                phoneNumberInput.val(request.customer_phone);
                //$('.answer-button').prop("disabled", false);

                callInfoLabel.append(str_injection);

            }

        });
        channel_phonecalls.bind('phone-calls', function(notification) {

            console.log("*******************************");
            console.log(notification.element.call);

            let newContainerCall = '<div callsid="' + notification.element.call.call_sid +
                '" id="accordion acordion-first">' +
                '<span>' +
                '<li class="list-group-item list-group-item-sin-border" id="list-' + notification.element.call.id +
                '" data-toggle="collapse" href="#collapseExample" call-direction="' + notification.element.call
                .direction + '" aria-expanded="false" aria-controls="#collapse-' + notification.element.call
                .direction + '" element-id="' + notification.element.call.id + '" onClick="styles($(this))">' +
                '<div class="container-list-calls active" id="info-section-' + notification.element.call.id + '">' +
                '<a class="btn-no-padding" data-toggle="collapse" data-target="#collapse-' + notification.element
                .call.id + '" aria-expanded="false" aria-controls="collapse-' + notification.element.call.id +
                '">' +
                '<div class="container-status-icon">' +
                '<img class="img-status-call" src="img-oficial/' + notification.element.call.end_status + '-' +
                notification.element.call.direction + '.svg" />' +
                '</div>' +
                '<div class="container-num-hr">' +
                '<div class="container-num" id="call_sid-' + notification.element.call.call_sid + '">' +
                '<span class="num-phone">' + ((notification.element.call.direction == 'inbound') ? ((notification
                        .element.call.customer_name != '') ? notification.element.call.customer_name :
                    notification.element.call.from) : notification.element.call.to) +
                '</span>' +
                '</div>' +
                '<div class="container-hr-call">' +
                '<span class="hr-call">' + notification.element.call.created_at_formated + '</span>' +
                '</div>' +
                '</div>' +
                '<div class="container-status-description">' +
                '<div class="container-status">' +
                '<span class="status-description">' + lbl_status[notification.element.call.end_status] + '</span>' +
                '</div>' +
                '<div class="container-badge">' +
                '<span class="badge badge-' + lbl_status_color[notification.element.call.end_status] +
                ' float-right">' + lbl_status[notification.element.call.through] + ' None</span>' +
                '</div>' +
                '<div class="container-agent">' +
                '<!-- <span class=agent>Rodrigo</span> -->' +
                '</div>' +
                '</div>' +
                '</a>' +
                '</div>' +
                '<div id="collapse-' + notification.element.call.id +
                '" class="collapse" data-parent=".body-wrapper" role="button">' +
                '<div class="card-body text-center" id="collapse-section-' + notification.element.call.id + '">' +
                notification.element.call.updated_at +
                '<br>' +
                /*'<div class="holder-parent">' +
                  '< div class="audio green-audio-player">' +
                  '<div class="play-pause-btn"> ' +
                  '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="20" viewBox="0 0 18 24">' +
                  '<path fill="#566574" fill-rule="evenodd" d="M18 12L0 24V0" class="play-pause-icon" id="playPause"/>' +
                  '</svg>'+
                  '</div>'+
                  '<audio crossorigin>'+
                  '<source src='+ notification.element.call.RecordingUrl +' type="audio/mpeg">' +
                  '</audio>'+
                  '</div>' +
                  '</div>' + */
                '<br>' +
                '<div class="btn-toolbar justify-content-center" role="toolbar" aria-label="Toolbar with button groups">' +
                '<div class="btn-group " role="group" aria-label="first group">' +
                '<button type="button" class="btn btn-primary2 mx-5" id="llamar- ' + notification.element.call.id +
                ' ">' +
                '<i data-count="0" class="fa fa-phone" style=""></i>' +
                '</button>' +
                '</div>' +
                '<div class="btn-group" role="group" aria-label="second group">' +
                '<button type="button" class="btn btn-success mx-5"><i class="bi bi-whatsapp"></i></button>' +
                '</div>' +
                '<div class="btn-group" role="group" aria-label="Third group">' +
                '<button type="button" class="btn btn-dark mx-5"><i class="bi bi-info-circle"></i></button>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</li>' +
                '</span>' +
                '</div>';

            let dateBagde = '<div id="bagde-' + notification.element.call.id + '" class="container-bagde-index">' +
                '<div class="fake-badge">' +
                '<span class="fake-badge-index">' + notification.element.call.created_at_comparative + '</span>' +
                '</div>' +
                '</div>';

            let fakeaBadgeIndex = $('#bagde-0 >.fake-badge > span').html();
            let idParentCallsid = $('notification.element.call.parentCallSid');

            let comparative = notification.element.call.created_at_comparative.replace(' ', '')

            var x;
            let callSid = $('div[callsid="' + notification.element.call.call_sid + '"]').length;
            if (callSid > 0) {
                $('div[callsid="' + notification.element.call.call_sid + '"]').remove();
                $('#bagde-' + notification.element.call.id + '').detach();
            }

            if (fakeaBadgeIndex == comparative) {
                x = $('#bagde-0').detach();
                $(".body-wrapper").prepend(newContainerCall);
                $(".body-wrapper").prepend(x);

            } else if (fakeaBadgeIndex != notification.element.call.created_at_comparative) {
                $(".body-wrapper").prepend(newContainerCall);
                $(".body-wrapper").prepend(dateBagde);

            }

        });

    </script>

</body>

</html>

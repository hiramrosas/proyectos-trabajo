<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\PanelNotification;
use App\User;
use App\CustomClass\Record;

use App\calls;
use Carbon\Carbon;

use setasign\Fpdi\TcpdfFpdi;// Like this
use Illuminate\Support\Facades\Auth;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function index()
    {

        $myfunctions = new Record();
        $res = $myfunctions->getHistory();

        $domain_request="";
        if(config('app.env')=='development'){
            $domain_request = "http://127.0.0.1:7000/v1/";
        }elseif(config('app.env')=='production'){
            $domain_request = "https://api.empenatuprenda.com/v1/";
        }else{
            $domain_request = "http://127.0.0.1:7000/v1/";
        }
        //error_log($res["array_lbl"][SUCCESSFUL_CALL]);
        return view('home')->with('domain_request',$domain_request)->with($res);

    }

    public function Admin() {
        $myfunctions = new Record();
        $res = $myfunctions->getHistory();

        return view('administrador')->with($res);
    }
    public function chat($search=null) {

        $myfunctions = new Record();
        $res = $myfunctions->getHistory();

        $domain_request="";
        if(config('app.env')=='development'){
            $domain_request = "http://127.0.0.1:7000/v1/";
        }elseif(config('app.env')=='production'){
            $domain_request = "https://api.empenatuprenda.com/v1/";
        }else{
            $domain_request = "http://127.0.0.1:7000/v1/";
        }

        $all_users = User::all();


        error_log($search);
        if ($search!="") {
            // set start
            $data = array (
                'api_address' => $domain_request,
                'search_word' => $search,
                'all_users'  => $all_users
            );
        }else{
            $data = array (
                'api_address' => $domain_request,
                'search_word' => "",
                'all_users'  => $all_users
            );
        }

        return view('chat')->with($data)->with($res);

    }
    /* Temporal */
    public function buscador(){

        $myfunctions = new Record();
        $res = $myfunctions->getHistory();

        return view('buscador')->with($res);
    }

    public function precios(){

        $myfunctions = new Record();
        $res = $myfunctions->getHistory();

      return view('precios')->with($res);
    }

    public function refrendos(Request $request){

        $myfunctions = new Record();
        $res = $myfunctions->getHistory();

        error_log($request->pawn_id);
        error_log($request->amount);
        error_log($request->charges);

        error_log($request->initial_date);
        error_log($request->last_date);
        error_log($request->limit_date);

        error_log($request->interest_id);
        error_log($request->customer_name);
        error_log($request->customer_email);
        error_log($request->customer_phone);
        error_log($request->order_desc);
        error_log($request->order_total);
        error_log($request->category_fk);


        $domain_request = env('API_ROUTE');
        $domain_landing = env('LANDING_ROUTE');

        $data = array (
            'api_address' => $domain_request,
            'landing_address' => $domain_landing,
            'pawn_id' => $request->pawn_id,
            'amount' => $request->amount,
            'charges' => $request->charges,
            'order_total' => $request->order_total,
            'initial_date' => $request->initial_date,
            'last_date' => $request->last_date,
            'limit_date' => $request->limit_date,
            'interest_id' => $request->interest_id,
            'customer_name' => $request->customer_name,
            'customer_email' => ($request->customer_email=="null")?"":$request->customer_email,
            'customer_phone' => $request->customer_phone,
            'has_interest_id' => (($request->interest_id=="null"||$request->interest_id=="")?false:true),
            'order_desc' => $request->order_desc,
            'category_fk' => $request->category_fk,
        );
        return view('refrendos')->with($data)->with($res);
    }

    public function contracts(Request $request){
        $myfunctions = new Record();
        $res = $myfunctions->getHistory();
        return view('contracts')->with($res);
    }
    public function contractsProccess(Request $request){

        error_log($request->contract_id);
        error_log($request->contract_date);

        $myfunctions = new Record();
        $res = $myfunctions->getHistory();

        $pdf = new TcpdfFpdi();
        $pdf->AddPage();
        $pdf->SetFont('helvetica','B','10');
        $path = public_path("contracts/template/base_contract_r_0.pdf");

        $pdf->setSourceFile($path);
        $tplId = $pdf->importPage(1);
        $pdf->useTemplate($tplId, null, null, null, 400, true);

        /* Tabla 1 de Excel*/
        $net_fee = $request->net_fee; //Interés neto
        $interest_rate = $net_fee * 12; //Interés
        $loan_amount = $request->loan_amount; //Monto del préstamo
        $months = $request->fixed_term_months; //Meses
        $fee_plus_iva = $net_fee * 1.16; //Interés neto más IVA
        $monthly_fee = $this->calculoRefrendo($fee_plus_iva, $loan_amount, $months);; //Cuota del mes


        /* Tabla 2 de Escel */
        $initial_balance = $loan_amount;  //Saldo inicial
        $fee = $this->calculoInteres($loan_amount, $net_fee); //Interés
        $iva = $this->calculoIVA($fee); //IVA
        $refrendo = $monthly_fee; //Por refrendo
        $capital = $this->calculoCapital($refrendo, $iva, $fee); //Capìtal
        $por_desempeno = $this->calculoPorDesempeno($loan_amount, $fee, $iva); //Por desempeño
        $ending_balance = $this->calculoSaldoFinal($loan_amount, $capital); //Saldo final

        // Second details
        $pdf->SetXY(206, 7.5);
        $pdf->Write(0.1, $request->contract_id);

        $pdf->SetXY(206, 11.5);
        $pdf->Write(0.1,$request->contract_date);

        $pdf->SetXY(36, 33);
        $pdf->Write(0.1,$request->contract_full_name);

        $pdf->SetXY(144, 33);
        $pdf->Write(0.1,$request->nat_id_type.' - '.$request->nat_id_number);

        $pdf->SetXY(8, 37);
        $pdf->Write(0.1,$request->contract_address);

        $pdf->SetXY(168, 37);
        $pdf->Write(0.1,$request->contract_phonenumber);

        $pdf->SetXY(8, 41);
        $pdf->Write(0.1,$request->contract_email);
        $pdf->SetXY(153, 41);
        $pdf->Write(0.1,$request->beneficiary_full_name);


        $pdf->SetXY(35, 45);
        $pdf->Write(0.1,$request->beneficiary_address);


        $pdf->SetXY(94, 71);
        $pdf->Write(0.1,$loan_amount);

        $pdf->SetXY(135, 72);
        $pdf->Write(0.1,$por_desempeno);

        // [X]
        switch ($request->contract_type) {
            case "ETP_MONTH":
                $contract_type_x = 44.2;
                break;
            case "ETP_WEEK":
                $contract_type_x = 119;
                break;
            case "ETP_FIXED":
                $contract_type_x = 85;
                break;
            case "ETP_COM":
                $contract_type_x = 160;
                break;
            default:
                $contract_type_x = 44.2;
        }

        $pdf->SetXY($contract_type_x, 94);
        $pdf->Write(0,"X");


        $last_section_y = 48.2;


        // Pre-last section
        $pdf->SetXY(49, 166+$last_section_y);
        $pdf->Write(0.1,"$".$request->pawn_valuation);

        //Payment
        switch ($request->beneficiary_payment_type) {
            case "PAY_CLABE":
            case "PAY_CARD":
                $contract_type_x = 108;
                break;
            case "PAY_CASH":
                $contract_type_x = 131;
                break;
            default:
                $contract_type_x = 108;
        }

        $pdf->SetXY($contract_type_x, 178.5+$last_section_y);
        $pdf->Write(0,"X");

        $pdf->SetXY(194, 178.5+$last_section_y);
        $pdf->Write(0, str_replace(' ', '',$request->beneficiary_bank_number));

        $pdf->SetXY(50, 182.5+$last_section_y);
        $pdf->Write(0, $request->beneficiary_bank_name);
        //beneficiary_bank_number
        //beneficiary_bank_name
        //Font MEDIUM
        $pdf->SetFont('');
        $pdf->SetFont('helvetica','B','8');

        $pdf->SetXY(8, 275+$last_section_y);
        $pdf->Write(0.1,$request->pawn_serie);

        $pdf->SetXY(39, 275+$last_section_y);
        $pdf->Multicell(34,2,$request->pawn_model);

        $pdf->SetXY(74, 272+$last_section_y);
        $pdf->Multicell(97,2,$request->pawn_characteristics);

        $pdf->SetXY(173, 275+$last_section_y);
        $pdf->Write(0,"$".$request->pawn_valuation);


        /* Fila inicial datos de la tabla */

        /*$fixedHeightY = 142;

        $pdf->SetXY(44, $fixedHeightY);
        $pdf->Write(0.1, $initial_balance);

        $pdf->SetXY(61, $fixedHeightY);
        $pdf->Write(0.1, $capital);

        $pdf->SetXY(87, $fixedHeightY);
        $pdf->Write(0.1, $fee);

        $pdf->SetXY(107, $fixedHeightY);
        $pdf->Write(0.1,$iva);

        $pdf->SetXY(126, $fixedHeightY);
        $pdf->Write(0.1, $refrendo);

        $pdf->SetXY(155, $fixedHeightY);
        $pdf->Write(0.1, $por_desempeno);

        $pdf->SetXY(188, $fixedHeightY);
        $pdf->Write(0.1, $ending_balance);*/

        //

        /* Filas datos de la tabla */

        $table = $this->matriz($loan_amount, $net_fee, $refrendo);
        dump($table);

        $filas = count($table);
        $columnas= max(array_map('count', $table));

        dump($filas);
        dump($columnas);

        $x = 33; $y = 138.5;
        for($j = 0; $j < $filas; $j++) {
            $y++;
            $y = $y + 3.5;
            //Columna de número de plazos
            $pdf->SetXY($x, $y);
            $pdf->Write(0.1, $table[$j][0]);
            //Columna de saldo inicial
            $pdf->SetXY($x + 9, $y);
            $pdf->Write(0.1, $table[$j][1]);
            //Columna de Capital
            $pdf->SetXY($x + 28, $y);
            $pdf->Write(0.1, $table[$j][2]);
            //Columna de Interés
            $pdf->SetXY($x + 50, $y);
            $pdf->Write(0.1, $table[$j][3]);
            //Columna de IVA
            $pdf->SetXY($x + 74, $y);
            $pdf->Write(0.1, $table[$j][4]);
            //Columna de Por Refrendo
            $pdf->SetXY($x + 93, $y);
            $pdf->Write(0.1, $table[$j][5]);
            //Columna de Por Desempeño
            $pdf->SetXY($x + 122, $y);
            $pdf->Write(0.1, $table[$j][6]);
            //Columna de SAldo Final
            $pdf->SetXY($x + 153, $y);
            $pdf->Write(0.1, $table[$j][7]);

        }

        // Last section
        $pdf->AddPage();
        $tplId = $pdf->importPage(2);
        $pdf->useTemplate($tplId, null, null, null, 400, true);

        $pdf->AddPage();
        $tplId = $pdf->importPage(3);
        $pdf->useTemplate($tplId, null, null, null, 400, true);

        // Now this showing as preview in browser
        // This is output
        // let's check now by running project. But before that we have to add Route.
        //error_log($_SERVER['DOCUMENT_ROOT'] . "pdf/white_out.pdf");
        // Because I is for preview for browser.
        //$pdf->Output('F',$_SERVER['DOCUMENT_ROOT'] . "pdf/white_out.pdf");


        $url_pdf = "/contracts/". Auth::user()->id ."_contrato.pdf";
        $result_pdf = $pdf->Output(realpath(__DIR__ . '/../../../public').$url_pdf, 'F');

        $data = array (
            'url_pdf' => $url_pdf,
            'file_name' => "contract_".$request->contract_id
        );

        return view('contracts')->with($res)->with($data);


    }

    //Cálculos para generación de la Tabla 2 de Excel

    public function calculoInteres($saldo_inicial, $interes_neto) {
        return $saldo_inicial * $interes_neto;
    }

    public function calculoIVA($interes) {
        return round(($interes * 0.16), 2);
    }

    public function calculoRefrendo($int_mas_iva, $prestamo, $meses) {
        //Fórmula de Excel "Por refrendo" =(O43*O44)/(1-POTENCIA(1/(1+O43),O45))
        $primerArgumento = $int_mas_iva * $prestamo;
        $potencia = (1/pow((1 + $int_mas_iva),$meses));
        $segundoArgumento = 1 - $potencia;
        $resultado = round(($primerArgumento / $segundoArgumento), 2);
        return $resultado;
    }

    public function calculoCapital($por_refrendo, $iva, $interes) {
        return $por_refrendo - ($iva + $interes);
    }

    public function calculoPorDesempeno($monto, $interes, $iva) {
        return $monto + ($interes + $iva);
    }

    public function calculoSaldoFinal($monto, $capital) {
        return $monto - $capital;
    }

    public function matriz($loan_amount, $net_fee, $monthly_fee) {

        $saldo_inicial = $loan_amount;  //Saldo inicial
        $interes = $this->calculoInteres($loan_amount, $net_fee); //Interés
        $iva = $this->calculoIVA($interes); //IVA
        $refrendo = $monthly_fee; //Por refrendo
        $capital = $this->calculoCapital($refrendo, $iva, $interes); //Capìtal
        $por_desempeno = $this->calculoPorDesempeno($loan_amount, $interes, $iva); //Por desempeño
        $saldo_final = $this->calculoSaldoFinal($loan_amount, $capital); //Saldo final

        $matrix = array(
            array("", "1"=>$saldo_inicial, $capital, $interes, $iva, $refrendo, $por_desempeno, $saldo_final),
            array("", "1"=>"", "", "", "", "", "", ""),
            array("", "1"=>"", "", "", "", "", "", ""),
            array("", "1"=>"", "", "", "", "", "", ""),
            array("", "1"=>"", "", "", "", "", "", ""),
            array("", "1"=>"", "", "", "", "", "", ""),
            array("", "1"=>"", "", "", "", "", "", ""),
            array("", "1"=>"", "", "", "", "", "", ""),
            array("", "1"=>"", "", "", "", "", "", ""),
            array("", "1"=>"", "", "", "", "", "", ""),
            array("", "1"=>"", "", "", "", "", "", ""),
            array("", "1"=>"", "", "", "", "", "", "")
        );

        $filas = count($matrix);
        $columnas= max(array_map('count', $matrix));
        $monthCounter = 1;

        for($j = 0; $j < $filas; $j++) {
            //Rellenar toda la columna de refrendos con el mismo valor que es "Cuota del mes"
            $matrix[$j][5] = $refrendo;
            //Asignar número de fila según los meses dados
            $matrix[$j][0] = $monthCounter + $j;
        }

        for($j = 1; $j < $columnas; $j++) {
            if($j != 0)
            //Actualizando la columna de saldo inicial que debe actualizarse con el saldo final de la fila anterior
            $matrix[$j][1] = round(($matrix[$j-1][7]), 2);
            //Actualizando la columna de interés con sus nuevos valores
            $matrix[$j][3] = round(($matrix[$j][1] * $net_fee),2);
            //Actualizando la columna de IVA con sus nuevos valores
            $matrix[$j][4] = round(($matrix[$j][3] * 0.16), 2);
            //Actualizando la columna de Capital con sus nuevos valores
            $matrix[$j][2] = round(($matrix[$j][5] - ($matrix[$j][4] + $matrix[$j][3])), 2);
            //Actualizando la columna de "Por desempeño" con sus nuevos valores
            $matrix[$j][6] = round(($matrix[$j][1] + ($matrix[$j][3] + $matrix[$j][4])), 2);
            //Actualizando la columna de "Saldo final" con sus nuevos valores
            $matrix[$j][7] = round(($matrix[$j][1] - $matrix[$j][2]), 2);
            if($matrix[$j][7] < 1)
            $matrix[$j][7] = 0;
        }

        //Valida que sólo se imprima parte de la matriz que corresponde a los meses ingresados
        $valid_matrix = array_filter($matrix, function($arr) {
            return $arr["1"] > 0;
        });

        $encodedMatrix = json_encode($valid_matrix);

        //var_dump(json_decode($encodedMatrix));

        $decodedMatrix = json_decode($encodedMatrix);

        return $valid_matrix;
    }
 }

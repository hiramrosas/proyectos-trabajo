@extends('layouts.master')
@section('title', 'Pago de refrendos')
@section('refrendos-active', 'menu-open')

@section('subtitle', 'Refrendos')

@section('head')
    <link rel="stylesheet" href="{{ asset('css/sidebar_calls.css') }}">
@stop
@section('content')
    <style type="text/css">
        .hide-notif::after {
            content: none;
        }

    </style>

    <!-- Ventana emergente para agregar nuevo precio de mercado -->
    <div class="modal fade" id="modal-refrendo">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="border-radius: 20px">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        &times;
                    </button>
                    <h5 id="prodTitle" class="modal-title">Agrega un nuevo precio de mercado</h5>
                </div>
                <form id="form-quote" class="">

                    <div class="modal-body row pt-4">

                        <div class="container col-12">
                            <div class="row">
                                <div class="controls col-12 ">

                                    <div class="row">
                                        <div class="col-4">
                                            <label>Fecha inicial:</label>
                                            <input id="initial_date" name="initial_date" type="text" class=" form-control "
                                                placeholder="10 digitos" style="display:block; width:100%"
                                                value="{{ $initial_date }}">
                                            <div id="initial_date_help" class="help-block"></div>
                                        </div>
                                        <div class="col-4">
                                            <label>Fecha ultimo pago:</label>
                                            <input id="last_date" name="last_date" type="text" class=" form-control "
                                                placeholder="10 digitos" style="display:block; width:100%"
                                                value="{{ $last_date }}">
                                            <div id="last_date_help" class="help-block"></div>
                                        </div>
                                        <div class="col-4">
                                            <label>Fecha limitie:</label>
                                            <input id="limit_date" name="limit_date" type="text" class=" form-control "
                                                placeholder="10 digitos" style="display:block; width:100%"
                                                value="{{ $limit_date }}">
                                            <div id="limit_date_help" class="help-block"></div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">

                                        <div class="col-6">
                                            <label>Id de refrendo: </label>
                                            <input id="interest_id" name="interest_id" type="text"
                                                placeholder="REF-ETPID-MM-YYYY" class="form-control "
                                                style="display:block; width:100%" value="{{ $interest_id }}">
                                            <div id="interest_id_help" class="help-block"></div>
                                        </div>
                                        <div class="col-6">
                                            <label>Id de contrato: </label>
                                            <input id="order_name" name="order_name" type="text" placeholder="ETPEMP"
                                                class="form-control " style="display:block; width:100%"
                                                value="{{ $pawn_id }}">
                                            <div id="order_name_help" class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-6">
                                            <label>Descripción: </label>
                                            <input id="order_desc" name="order_desc" type="text"
                                                placeholder="Detalles de articulo" class="form-control "
                                                style="display:block; width:100%" value="{{ $order_desc }}">
                                            <div id="order_desc_help" class="help-block"></div>
                                        </div>
                                        <div class="col-6">
                                            <label>Monto del refrendo:</label>
                                            <input id="order_subtotal" name="order_subtotal" class=" form-control "
                                                placeholder="0.00" style="display:block; width:100%"
                                                value="{{ $amount }}">
                                            <div id="order_subtotal_help" class="help-block"></div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <label>Cargos extra: </label>
                                            <input id="order_charges" name="order_charges" placeholder="0.00"
                                                class="form-control " style="display:block; width:100%"
                                                value="{{ $charges }}">
                                            <div id="order_charges_help" class="help-block"></div>
                                        </div>
                                        <div class="col-6">
                                            <label>Monto Total:</label>
                                            <input id="order_total" name="order_total" class=" form-control "
                                                placeholder="0.00" style="display:block; width:100%"
                                                value="{{ $order_total }}">
                                            <div id="order_total_help" class="help-block"></div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-6">
                                            <label>Nombre del cliente:</label>
                                            <input id="customer_name" type="text" name="customer_name"
                                                class=" form-control " placeholder="Nombre Apellidos"
                                                style="display:block; width:100%" value="{{ $customer_name }}">
                                            <div id="customer_name_help" class="help-block"></div>
                                        </div>
                                        <div class="col-6">
                                            <label>Email del cliente: </label>
                                            <input id="customer_email" name="customer_email" type="text"
                                                placeholder="email@ejemplo.com*" class="form-control "
                                                style="display:block; width:100%" value="{{ $customer_email }}">
                                            <div id="customer_email_help" class="help-block"></div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-6">
                                            <label>Telefono celular:</label>
                                            <input id="customer_phone" name="customer_phone" type="text" maxlength="10"
                                                class=" form-control " placeholder="10 digitos"
                                                style="display:block; width:100%" value="{{ $customer_phone }}">
                                            <div id="customer_phone_help" class="help-block"></div>
                                        </div>
                                          <div class="col-6">
                                            <label>Categoría:</label>
                                            <div class="form-group" >
                                                <div class="controls">
                                                    <select id="category_select" name="category_fk" class="form-control">
                                                        <option value="" selected disabled>---------- Selecciona la categoría ----------</option>
                                                    </select>
                                                    <div class="help-block"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>


                            </div>
                        </div>

                    </div>
                    <div class="modal-footer text-center col-12">
                        <button id="modalForm" type="submit" class=" btn btn-bold btn-pure btn-info col-10">Enviar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="box box-default">

        <div id="header" class="box-header with-border">
            <div class="row">
                <div class="col-12">
                    <h3 class="box-title">Pago de refrendos</h3>
                    <button id="precioNuevo" type="button" class="btn btn-sm btn-info float-right" data-toggle="modal"
                        data-target="#modal-refrendo">
                        <i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
        </div>

        <div class="box-body">
            <table id="refrendos-dt" class="table table-bordered table-striped table-responsive">
                <thead>
                    <tr style="background-color: #1d252d;">
                        <th>UUID</th>
                        <th>REF-ID</th>
                        <th>ETPEMP-ID</th>
                        <th>Descripción</th>
                        <th>Monto Refrendo</th>
                        <th>Monto Cargo</th>
                        <th>Total a pagar</th>
                        <th>Fecha limite</th>
                        <th>Mét. pago</th>
                        <th>Est. pago</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Tel.</th>
                        <th>Orden de pago</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>

    <script type="text/javascript">
        var objApiRequest = {};
        var data_serialized = {};
        var api_address = "{{ $api_address }}";
        var activate_modal = "{{ $has_interest_id }}";
        $(document).ready(function() {
            $("#form-quote").submit(function(e) {
                submitForm(e);
            });

            if (activate_modal) {
                $('#modal-refrendo').modal('show');
            }

            var settings = {
                "async": true,
                "crossDomain": true,
                "url": api_address + "payments/interest/get-all",
                "method": "GET"
            };

            $.ajax(settings).fail(function(jqXHR, tectStatus, errorThrown) {
                console.log('Error');
                //errorSubmit();
            }).done(function(response) {
                console.log(response);
                //$('#modal-refrendo').modal( 'hide' );
                $.each(response, function(index, element) {

                    var formattedDate = new Date(element.limit_date);
                    var d = formattedDate.getDate();
                    var m = formattedDate.getMonth();
                    m += 1; // JavaScript months are 0-11
                    var y = formattedDate.getFullYear();
                    var str_date = d + '/' + m + '/' + y;

                    var str_append = '<tr>' +
                        '<td id="' + element.uuid + '-id" value="' + element.uuid + '">' + element
                        .uuid + '</td>' +
                        '<td id="' + element.uuid + '-id" value="' + element.interest_id + '">' +
                        element.interest_id + '</td>' +
                        '<td id="' + element.uuid + '-order" value="' + element.order_name + '">' +
                        element.order_name + '</td>' +
                        '<td id="' + element.uuid + '-desc" value="' + element.order_desc + '">' +
                        element.order_desc + '</td>' +
                        '<td id="' + element.uuid + '-subtotal" value="' + (element.order_subtotal /
                            100) + '">$' + (element.order_subtotal / 100) + '</td>' +
                        '<td id="' + element.uuid + '-charges" value="' + (element.order_charges /
                            100) + '">$' + (element.order_charges / 100) + '</td>' +
                        '<td id="' + element.uuid + '-total" value="' + (element.order_total /
                        100) + '">$' + (element.order_total / 100) + '</td>' +
                        '<td id="' + element.uuid + '-date" value="' + str_date + '">' + str_date +
                        '</td>' +
                        '<td id="' + element.uuid + '-payment-method" value="' + element
                        .payment_method + '">' + element.payment_method + '</td>' +
                        '<td id="' + element.uuid + '-status" value="' + element.status + '">' +
                        element.status + '</td>' +
                        '<td id="' + element.uuid + '-name" value="' + ((element.customer_name)
                            .split(" "))[0] + '">' + element.customer_name + '</td>' +
                        '<td id="' + element.uuid + '-email" value="' + element.customer_email +
                        '">' + element.customer_email + '</td>' +
                        '<td id="' + element.uuid + '-phonenumber" value="' + element
                        .customer_phone + '">' + element.customer_phone + '</td>' +
                        '<td id="' + element.uuid +
                        '-url"><a href="{{ $landing_address }}pagar-refrendo/' + element.uuid +
                        '" target="_blank" class="text-info">Ver orden<a/></td>' +
                        '<td id="' + element.uuid + '-actions">' +
                        '   <div class="input-group-btn">' +
                        '     <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Acciones</button>' +
                        '      <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 37px, 0px); top: 0px; left: 0px; will-change: transform;">' +
                        '        <a class="dropdown-item text-warning" href="#" onclick="sendWANotification(\'' +
                        element.uuid +
                        '\')"><i id="" class="fa fa-bell hide-notif" style="content: none;"></i> Notificar</a>' +
                        '        <div class="dropdown-divider" style="border-top: 1px solid #35393e;"></div>' +
                        '        <a class="dropdown-item text-success" href="#"><i id="" class="fa fa-check"></i> Aprovar</a>' +
                        '        <a class="dropdown-item text-danger" href="#"><i id="" class="fa fa-close"></i> Cancelar</a>' +
                        '      </div>' +
                        '    </div>' +
                        '</td>' +
                        '</tr>';
                    $('#refrendos-dt').append(str_append);
                });

                var table = $('#refrendos-dt').DataTable({
                    "pageLength": 50,
                    lengthMenu: [
                        [50, 100, 200, 500, 1000, -1],
                        [50, 100, 200, 500, 1000, 'Todos']
                    ],
                    dom: 'lBfrtip',
                    "order": [
                        [8, "desc"]
                    ],
                    buttons: [
                        'csv', 'excel'
                    ]
                });
                //new $.fn.dataTable.FixedHeader( table );


            });

            //$("#form-quote").submit(submitForm(event));
        });

        function submitForm(event) {
            //alert('submit intercepted');
            event.preventDefault();
            data_serialized = $("#form-quote").serializeArray();

            //data_serialized.order_total = data_serialized.order_subtotal+data_serialized.order_charges;
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": api_address + "payments/interest/create",
                "method": "POST",
                "data": data_serialized
            }

            console.log(data_serialized);

            $.ajax(settings).fail(function(jqXHR, tectStatus, errorThrown) {
                console.log('Error');
                //errorSubmit();
            }).done(function(response) {
                console.log(response);
                $('#modal-refrendo').modal('hide');
                window.location.replace("/refrendos");
            });
        }

        function sendWANotification(uuid) {
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "/api/messages/whatsapp/post-template",
                "method": "POST",
                "data": {
                    "id_request": $('#' + uuid + '-order').attr('value'),
                    "phone_number": $('#' + uuid + '-phonenumber').attr('value'),
                    "type": 'INTEREST_NOTIF',
                    "customer_name": $('#' + uuid + '-name').attr('value'),
                    "article": $('#' + uuid + '-desc').attr('value'),
                    "customer_email": $('#' + uuid + '-email').attr('value'),
                    "request_uuid": uuid,
                    "request_amount": $('#' + uuid + '-total').attr('value'),
                    "request_date": $('#' + uuid + '-date').attr('value')
                }
            }
            console.log(settings);

            $.ajax(settings).fail(function(jqXHR, tectStatus, errorThrown) {
                console.log('Error');
                //errorSubmit();
            }).done(function(response) {
                console.log(response);
            });
        }
    </script>

    <script>
        var settings = {
            "url": "https://api.empenatuprenda.com/v1/category",
            "method": "GET",
            "timeout": 0,
        };

        $.ajax(settings).done(function (response) {
            //console.log(response);
            $.each(response, function( index, category ) {
                //console.log(category.category_title);
                $('#category_select').append(
                    '<option value="'+category.id+'">'+category.category_title+'</option>'
                    )});
                });
    </script>
@stop

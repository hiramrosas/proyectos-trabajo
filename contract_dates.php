<?php

namespace App\Traits;

use DateTime;
use DateInterval;

trait ContractGeneratorLogic {

    //Cálculos para generación de la Tabla 2 de Excel

    protected function calculoInteres($saldo_inicial, $interes_neto) {
        return $saldo_inicial * $interes_neto;
    }

    protected function calculoIVA($interes) {
        return round(($interes * 0.16), 2);
    }


    protected function calculoRefrendo($int_mas_iva, $prestamo, $meses) {
        //Fórmula de Excel "Por refrendo" =(O43*O44)/(1-POTENCIA(1/(1+O43),O45))
        $primerArgumento = $int_mas_iva * $prestamo;
        $potencia = (1/pow((1 + $int_mas_iva),$meses));
        $segundoArgumento = 1 - $potencia;
        $resultado = round(($primerArgumento / $segundoArgumento), 2);
        return $resultado;
    }

    protected function calculoCapital($por_refrendo, $iva, $interes) {
        return $por_refrendo - ($iva + $interes);
    }

    protected function calculoPorDesempeno($monto, $interes, $iva) {
        return $monto + ($interes + $iva);
    }

    protected function calculoSaldoFinal($monto, $capital) {
        return $monto - $capital;
    }

    protected function calculoFecha($fecha_contrato) {
        //date_default_timezone_set('UTC');
        $originalDate = $fecha_contrato;
        //original date is in format YYYY-mm-dd in PHP american style
        $timestamp = strtotime($originalDate);
        $newDate = date("d-m-Y", $timestamp);
        return $newDate;
    }

    protected function diaPago($fecha_contrato) {
        $timestamp = strtotime($fecha_contrato);
        $date = date('d-m-Y', $timestamp);
        $day = date('d', $timestamp);
        if($date == date("29-01-Y") || $date == date("28-02-Y") || $date == date("31-01-Y") || $date == date("31-03-Y") || $date == date("31-05-Y") ||
        $date == date("31-07-Y") || $date == date("31-08-Y") || $date == date("31-10-Y") || $date == date("31-12-Y")) {
               $day = "30";
        }
        return $day;
    }

    protected function prestamoSobreAvaluo($avaluo, $prestamo) {
        $porcentaje = $prestamo / $avaluo;
        $str = strval($porcentaje);
        $formatter = round(((float)$str * 100 ), 2) . '%';
        return $formatter;
    }

    // "Opciones de pago para refrendo o desempeño" Tabla basada en el archivo de Excel 'Calculadoras' => 'Plazos Fijos'
    protected function matriz($loan_amount, $net_fee, $monthly_fee, $fecha_contrato, $meses) {

        $saldo_inicial = $loan_amount;  //Saldo inicial
        $interes = $this->calculoInteres($loan_amount, $net_fee); //Interés
        $iva = $this->calculoIVA($interes); //IVA
        $refrendo = $monthly_fee; //Por refrendo
        $capital = $this->calculoCapital($refrendo, $iva, $interes); //Capìtal
        $por_desempeno = $this->calculoPorDesempeno($loan_amount, $interes, $iva); //Por desempeño
        $saldo_final = $this->calculoSaldoFinal($loan_amount, $capital); //Saldo final
        $fecha_pago = $this->calculoFecha($fecha_contrato); //Fecha de inicio del contrato

        if($meses == 1) {
            $refrendo = $interes + $iva;
        }

        //Excepciones para el correcto cálculo de las fechas cuando el +1 mes se "rompe"
        switch($fecha_pago) {
            case date("30-01-Y"):
                $siguienteFecha = date("d-m-Y", strtotime($fecha_pago."+ 29 days"));
                break;
            case date("31-01-Y"):
                $siguienteFecha = date("d-m-Y", strtotime($fecha_pago."+ 28 days"));
                break;
            case date("29-01-Y"):
            case date("31-03-Y"):
            case date("31-05-Y"):
            case date("31-08-Y"):
            case date("31-10-Y"):
                $siguienteFecha = date("d-m-Y", strtotime($fecha_pago."+ 30 days"));
                break;
            default:
            $siguienteFecha = date("d-m-Y", strtotime($fecha_pago."+ 1 month"));
            break;
        }

        $initialDay = date("d",strtotime($fecha_pago));
        dump($initialDay);

        $matrix = array(
            array("1", "1"=>$saldo_inicial, $capital, $interes, $iva, $refrendo, $por_desempeno, $saldo_final, $siguienteFecha),
            array("", "1"=>"", "", "", "", "", "", "", ""),
            array("", "1"=>"", "", "", "", "", "", "", ""),
            array("", "1"=>"", "", "", "", "", "", "", ""),
            array("", "1"=>"", "", "", "", "", "", "", ""),
            array("", "1"=>"", "", "", "", "", "", "", ""),
            array("", "1"=>"", "", "", "", "", "", "", ""),
            array("", "1"=>"", "", "", "", "", "", "", ""),
            array("", "1"=>"", "", "", "", "", "", "", ""),
            array("", "1"=>"", "", "", "", "", "", "", ""),
            array("", "1"=>"", "", "", "", "", "", "", ""),
            array("", "1"=>"", "", "", "", "", "", "", "")
        );

        $filas = count($matrix);
        $columnas= max(array_map('count', $matrix)); //Por ahora sin uso
        $monthCounter = 1;

        for($j = 1; $j < $filas; $j++) {
            //Asignar número de fila según los meses dados
            $matrix[$j][0] = $monthCounter + $j;
            if($j != 0)
            //Rellenar toda la columna de refrendos con el mismo valor que es "Cuota del mes"
            $matrix[$j][5] = $refrendo;
            //Actualizando la columna de saldo inicial que debe actualizarse con el saldo final de la fila anterior
            $matrix[$j][1] = round(($matrix[$j-1][7]), 2);
            //Actualizando la columna de interés con sus nuevos valores
            $matrix[$j][3] = round(($matrix[$j][1] * $net_fee),2);
            //Actualizando la columna de IVA con sus nuevos valores
            $matrix[$j][4] = round(($matrix[$j][3] * 0.16), 2);
            //Actualizando la columna de Capital con sus nuevos valores
            $matrix[$j][2] = round(($matrix[$j][5] - ($matrix[$j][4] + $matrix[$j][3])), 2);
            //Actualizando la columna de "Por desempeño" con sus nuevos valores
            $matrix[$j][6] = round(($matrix[$j][1] + ($matrix[$j][3] + $matrix[$j][4])), 2);
            //Actualizando la columna de "Saldo final" con sus nuevos valores
            $matrix[$j][7] = round(($matrix[$j][1] - $matrix[$j][2]), 2);
            if($matrix[$j][7] < 1)
            $matrix[$j][7] = 0;

            $date = date("d-m-Y", strtotime($this->calculoFecha($matrix[$j-1][8])));
            dump($date);
            $currentMonth = date("m",strtotime($date));
            $lastDayOfMonth = date("t",strtotime($date));
            $nextMonth = date("m",strtotime($date."+1 month"));
            if($currentMonth==$nextMonth-1 && (date("j",strtotime($date)) != $lastDayOfMonth)) {
                $nextDate = date("d-m-Y",strtotime($date." +1 month"));
            } else {
                $nextDate =
                date('d-m-Y', strtotime("last day of next month", strtotime($date)));
            }

            /*$lastDay = date("d",strtotime($nextDate));
            dump($lastDay);
            if($initialDay == "31") {
                $lastDay = "30";
                $nextDate =  date("$lastDay-m-Y", strtotime($nextDate));
            } else {
                $nextDate =  date("$initialDay-m-Y", strtotime($nextDate));
            }*/

            $matrix[$j][8] = $nextDate;

            /*$matrix[$j][8] = (
                date("d-m-Y", strtotime($this->calculoFecha($matrix[$j-1][8]))) == date("29-01-Y") ||
                date("d-m-Y", strtotime($this->calculoFecha($matrix[$j-1][8]))) == date("30-01-Y") ||
                date("d-m-Y", strtotime($this->calculoFecha($matrix[$j-1][8]))) == date("31-01-Y") ?
                date("28-02-Y") :
                date("d-m-Y", strtotime($this->calculoFecha($matrix[$j-1][8])."+ 1 month"))
            );*/

            //Actuaizando la siguiente fecha de pago
            /*$day = null;
            if($matrix[$j][8] == date("31-01-Y")) {
                $day = 28;
            } else {
                $day = 30;
            }*/

            /*$matrix[$j][8] = (
                date("d-m-Y", strtotime($this->calculoFecha($matrix[$j-1][8]))) == date("28-02-Y") ||
                date("d-m-Y", strtotime($this->calculoFecha($matrix[$j-1][8]))) == date("29-02-Y") ||
                //date("d-m-Y", strtotime($this->calculoFecha($matrix[$j-1][8]))) == date("31-m-Y") || //En teoría aquí debería funcionar para todos los meses con día 31, pero no. Abajo manualmente puse las excepciones siguientes
                date("d-m-Y", strtotime($this->calculoFecha($matrix[$j-1][8]))) == date("31-03-Y") ||
                date("d-m-Y", strtotime($this->calculoFecha($matrix[$j-1][8]))) == date("31-05-Y") ||
                date("d-m-Y", strtotime($this->calculoFecha($matrix[$j-1][8]))) == date("31-07-Y") ||
                date("d-m-Y", strtotime($this->calculoFecha($matrix[$j-1][8]))) == date("31-08-Y") ||
                date("d-m-Y", strtotime($this->calculoFecha($matrix[$j-1][8]))) == date("31-10-Y") ?
                date("d-m-Y", strtotime($this->calculoFecha($matrix[$j-1][8])."+ 30 days")) :
                date("d-m-Y", strtotime($this->calculoFecha($matrix[$j-1][8])."+ 1 month"))
            );*/

            /*$matrix[$j][8] = (
                date("d-m-Y", strtotime($this->calculoFecha($matrix[$j-1][8]))) == date("29-01-Y") ||
                date("d-m-Y", strtotime($this->calculoFecha($matrix[$j-1][8]))) == date("30-01-Y") ||
                date("d-m-Y", strtotime($this->calculoFecha($matrix[$j-1][8]))) == date("31-01-Y") ?
                date("28-02-Y") :
                date("d-m-Y", strtotime($this->calculoFecha($matrix[$j-1][8])."+ 1 month"))
            );*/

        }

        //Valida que sólo se imprima parte de la matriz que corresponde a los meses ingresados
        $valid_matrix = array_filter($matrix, function($arr) {
            return $arr["1"] > 0;
        });

        $encodedMatrix = json_encode($valid_matrix);

        $decodedMatrix = json_decode($encodedMatrix);

        return $decodedMatrix;
    }

}

